    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="profile.html"><img src="<?php echo base_url();?>img/ui-sam.jpg" class="img-circle" width="80"></a></p>
          <h5 class="centered">Eduardo Langarica</h5>
          <li class="mt">
            <a href="index.php/welcome/index">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
              </a>
          </li>
          <li class="sub-menu">
            <a >
              <i class="fa fa-file-text-o"></i>
              <span>Gestion de Publicaciones</span>
              </a>
            <ul class="sub">
              <li><a class="fa fa-calendar-o" href="index.php/Ccalendar">&nbsp;&nbsp;Calendario</a></li>
              <li><a class="fa fa-exclamation-circle" href="index.php/Welcome/pendientes">&nbsp;&nbsp;Pendientes</a></li>
              <li><a class="fa fa-check-square" href="index.php/Welcome/aceptadas">&nbsp;&nbsp;Aceptadas</a></li>
              <li><a class="fa fa-times-circle" href="index.php/Welcome/rechazadas">&nbsp;&nbsp;Rechazadas</a></li>
            </ul>
          </li>
          <li class="menu">
            <a href="index.php/Welcome/campana">
              <i class="fa fa-list-alt"></i>
              <span>Campañas</span>
              </a>

          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-sitemap"></i>
              <span>Red Semantica</span>
              </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-list-alt"></i>
              <span>Analitica</span>
              </a>
            <ul class="sub">
              <li><a class="fa fa-file-text" href="index.php/Welcome/feed">&nbsp;&nbsp;Feed Pagina</a></li>
              <li><a class="fa fa-bar-chart-o" href="index.php/Welcome/comentarios">&nbsp;&nbsp;Estadisticas Comentarios</a></li>
              <li><a class="fa fa-bar-chart-o" href="index.php/Welcome/likes">&nbsp;&nbsp;Estadisticas Likes</a></li>
              <li><a class="fa fa-bar-chart-o" href="index.php/Welcome/shares">&nbsp;&nbsp;Estadisticas Shares</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-comments"></i>
              <span>Chat</span>
              </a>
            <ul class="sub">
            </ul>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************