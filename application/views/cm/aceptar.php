<?php include '../layout/header.php';  ?>
<?php include '../CM/menu.php';  ?>

<?php  
    $cod=$_GET["parametro"];

    require('../conexion.php');
    $query = "SELECT * FROM pendientes WHERE id=$cod";
    $resultado=$mysqli->query($query);

?>

    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <ol class="breadcrumb text-left">
                            <li><a href="#">Gestión de Publicaciones</a></li>
                            <li><a href="../../Main_app/CM/pendientes.php">Pendientes</a></li>
                            <li class="active">Aceptar</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6" style="margin:0 auto">
                <div class="card">
                  <div class="card-header"><strong>Aceptar Publicación</strong></div>
                  <div class="card-body">
                    <form class="form-horizontal" action="../Controlador/aceptado.php" method="POST">
                    <?php foreach ($resultado as $row ) {?>
                        <div class="row form-group">
                          <div class="col-8">
                            <div class="form-group"><label for="id" class=" form-control-label">ID</label><input type="text" id="id" name="id" value="<?php echo $cod;?>" class="form-control"></div>
                          </div>
                          <div class="col-8">
                            <div class="form-group"><label for="title" class=" form-control-label">Nombre</label><input type="text" id="title" name="title" value="<?php echo $row['nombre']; ?>" class="form-control"></div>
                          </div>
                          <div class="col-8">
                            <div class="form-group"><label for="descripcion" class=" form-control-label">Descripción</label><input type="text" id="descripcion" name="descripcion" value="<?php echo $row['descripcion']; ?>" class="form-control"></div>
                          </div>
                          <div class="col-8">
                            <div class="form-group"><label for="start" class=" form-control-label">Inicio</label><input type="text" id="start" name="start" value="<?php echo $row['inicio']; ?>" class="form-control"></div>
                          </div>
                          <div class="col-8">
                            <div class="form-group"><label for="end" class=" form-control-label">Fin</label><input type="text" id="end" name="end" value="<?php echo $row['final']; ?>" class="form-control"></div>
                          </div>
                        </div>
                    <?php } ?>
                      <div class="col-sm-offset-2 col-sm-10" align="center">
                      <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-dot-circle-o"> Aceptar</i></button>
                      <a href="pendientes.php" class="btn btn-danger btn-sm"><i class="fa fa-ban"></i> Cancelar</a>
                    </div>
                    </form>

                  </div><!-- .card-body -->
                </div><!-- .card -->
              </div><!-- .col-lg-6 -->
            </div><!-- .row -->
        <!-- /row -->
        </div>
        <!-- /row -->
      </section>
    </section>
    <!--main content end-->
<?php include '../layout/footer.php';  ?>