    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <ol class="breadcrumb text-left">
                            <li><a href="#">Gestión de Publicaciones</a></li>
                            <li class="active">Rechazadas</li>
                        </ol>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <h3 style="margin-left: 20px;">Publicaciones Rechazadas</h3>
            <br>
            <br>
            <div class="row">
                <div class="col-md-10">
              <table class="table table-bordered table-striped" style="margin: 0 auto; margin-left: 80px;">
                        <tr>
                            <th>ID</th>
                            <th>Titulo</th>
                            <th>Descripcion</th>
                            <th>Inicio</th>
                            <th>Final</th>
                        </tr>
                       <tbody>
                            <?php
                                    for($i=0;$i<count($rechazadas);$i++){  ?>
                                    <tr>
                                    <td><?php echo $rechazadas[$i]['id']; ?></td>
                                    <td><?php echo $rechazadas[$i]['title']; ?></td>
                                    <td><?php echo $rechazadas[$i]['descripcion']; ?></td>
                                    <td><?php echo $rechazadas[$i]['start']; ?></td>
                                    <td><?php echo $rechazadas[$i]['end']; ?></td>          
                                    </tr>
                                    <?php }  ?> 
                        </tbody> 
                    </table>                
                    <br>
                    <br>
                    <br>
                    <br>
                </div>
            </div>

        <!-- /row -->
        </div>
        <!-- /row -->
      </section>
    </section>
    <!--main content end-->