   <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
            <h3 style="margin-left: 20px;">Dashboard</h3>
          <div class="col-lg-12 main-chart">
            <div class="row mt">
              <!-- SERVER STATUS PANELS -->
              <div class="col-md-4 col-sm-4 mb">
                <div class="grey-panel pn donut-chart">
                  <div class="grey-header">
                    <h5>Pendientes</h5>
                  </div>
                  <br>
                  <br>
                  <a class="fa fa-exclamation-triangle fa-5x" href=""></a>
                  <br>
                  <br>
                  <div class="row">
                    <div class="col-sm-12 col-xs-12">
                      <h2>4</h2>
                    </div>
                  </div>
                </div>
                <!-- /grey-panel -->
              </div>
              <!-- /col-md-4-->
              <div class="col-md-4 col-sm-4 mb">
                <div class="darkblue-panel pn">
                  <div class="darkblue-header">
                    <h5>Mensajes</h5>
                  </div>
                  <br>
                  <br>
                  <a class="fa fa-envelope fa-5x" href="index.php/Welcome/test"></a>
                  <br>
                  <br>
                  <div class="row">
                    <div class="col-sm-12 col-xs-12">
                      <h2>2</h2>
                    </div>
                  </div>
                  <!-- <p>April 17, 2014</p> -->
<!--                   <footer>
                    <div class="pull-left">
                      <h5><i class="fa fa-hdd-o"></i> 17 GB</h5>
                    </div>
                    <div class="pull-right">
                      <h4>60% Used</h4>
                    </div>
                  </footer> -->
                </div>
                <!--  /darkblue panel -->
              </div>
              <!-- /col-md-4 -->
              <div class="col-md-4 col-sm-4 mb">
                <!-- REVENUE PANEL -->
                <div class="green-panel pn">
                  <div class="green-header">
                    <h5>Evolucion de Campañas</h5>
                  </div>
                  <div class="chart mt">
                    <div class="sparkline" data-type="line" data-resize="true" data-height="75" data-width="90%" data-line-width="1" data-line-color="#fff" data-spot-color="#fff" data-fill-color="" data-highlight-line-color="#fff" data-spot-radius="4" data-data="[200,135,667,333,526,996,564,123,890,464,655]"></div>
                  </div>
                  <br>
                  <br>
                  <div class="row">
                    <div class="col-sm-12 col-xs-12">
                      <h2>6</h2>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /col-md-4 -->
            </div>
            <!-- /row -->
        </div>
        <!-- /row -->
        </div>
        <!-- /row -->
      </section>
    </section>
    <!--main content end-->