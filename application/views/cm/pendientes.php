<!-- 
<script>
    function modificar(cod)
    {
        window.location="http://127.0.0.1/mkting/Main_app/CM/aceptar.php?parametro="+cod;
    }
        function rechazar(cod)
    {
        window.location="http://127.0.0.1/mkting/Main_app/CM/rechazar.php?parametro="+cod;
    }
</script> -->

    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <ol class="breadcrumb text-left">
                            <li><a href="#">Gestión de Publicaciones</a></li>
                            <li class="active">Pendientes</li>
                        </ol>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <h3 style="margin-left: 20px;">Publicaciones Pendientes</h3>
            <br>
            <br>
            <div class="row">
               <div class="col-md-10">
                <table class="table table-bordered table-striped" style="margin: 0 auto; margin-left: 80px;">
                        <tr>
                            <th>ID</th>
                            <th>Titulo</th>
                            <th>Descripcion</th>
                            <th>Inicio</th>
                            <th>Final</th>
                            <th>Aceptar</th>
                            <th>Rechazar</th>
                        </tr>
                        <tbody>
                            <?php
                                    for($i=0;$i<count($pendientes);$i++){  ?>
                                    <tr>
                                    <td><?php echo $pendientes[$i]['id']; ?></td>
                                    <td><?php echo $pendientes[$i]['nombre']; ?></td>
                                    <td><?php echo $pendientes[$i]['descripcion']; ?></td>
                                    <td><?php echo $pendientes[$i]['inicio']; ?></td>
                                    <td><?php echo $pendientes[$i]['final']; ?></td>
                                    <td><center><span class="fa fa-check-circle"></span></center></td>
                                    <td><center><span class="fa fa-times"></span></center></td>          
                                    </tr>
                                    <?php }  ?> 
                        </tbody>         
                    </table>
                    <br>
                    <br>
                    <br>
                    <br>
               </div>
            </div>
        <!-- /row -->
        </div>
        <!-- /row -->
      </section>
    </section>
    <!--main content end-->
