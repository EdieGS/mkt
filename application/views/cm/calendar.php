<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<script>
		$(document).ready(function() {
			$('#calendar').fullCalendar({
				option: {
					locale: 'es'
				},
				events: '<?php echo site_url("calendario/events"); ?>',
			});
		});
	</script>
	<style>

	  body {
	    margin: 60px 10px;
	    margin-left: 300px;
	    padding: 20px;
	    font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
	    font-size: 14px;
	  }

	  #calendar {
	    max-width: 600px;
	    margin: 0 auto;
	  }

	</style>
</head>
<body>
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
    *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">

        <h3>Calendario</h3>
        <!-- page start-->
        <div class="row">
               <div id='calendar'></div>
       </div>
        <!-- page end-->

    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->

</body>
</html>
