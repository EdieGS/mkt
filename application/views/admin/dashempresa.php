<?php $empresas = $this->session->userdata('empresas'); ?>

<!DOCTYPE html>
<html>
<head>
    
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <script src="main.js"></script>
</head>
<body>
<section id="main-content">

      <section class="wrapper">
        <div class="row">
            <div class="col-lg-12 main-chart">
         
            <div align="center"><h2>Gestión de Empresas</h2></div>
            <a href="" class="btn btn-primary" data-toggle="modal" data-target="#myModal"> Nueva Empresa</a>
            <table class="table">
            <thead>
                    <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Nombre de Empresa</th>
                    <th scope="col">Encargado</th>
                    <th scope="col">Telefono</th>
                    <th scope="col">E-mail</th>
                    <th title="Ajustes" scope="col"><img src="../plantilla/img/img2.png"></th>  
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($empresas as $row){?>
                    <tr>
                        <td><?php echo $row['id']?></td>
                        <td><?php echo $row['nombre']; ?></td>
                        <td><?php echo $row['encargado']; ?></td>
                        <td><?php echo $row['telefono'] ?></td>
                        <td><?php echo $row['e-mail'] ?></td>
                      
                        <td>
                            <a href="" class="btn btn-info">Editar</a>
                            <a href="" class="btn btn-danger">Eliminar</a>
                        </td>    
                    </tr>
                <?php } ?>
                </tbody>
                </table>

                        </div>
                        </div>   
<div class="container">
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Nueva Empresa</h4>
        </div>
        <div class="container">
        
            <h2>Ingresar Datos</h2>
                <form class="form-horizontal" action="/action_page.php">
                    <div class="row">
                        <label class="control-label col-sm-1" for="email"><b>Nombre:</b></label>
                        <div class="col-md-5">
                            <input type="email" class="form-control" id="email" placeholder="Ingresa nombre" name="email">
                        </div>
                    </div>
    
                    <div class="row">
                    <br>
                        <label class="control-label col-sm-1" for="fecha-ini"><b>Encargado:</b>  </label>
                        <div class="col-md-2">
                            <input type="email" class="form-control" id="email" placeholder="Ingresa encargado" name="email">
                        </div>
                        <label class="control-label col-sm-1" for="fecha-fin"><b>Telefono:</b>  </label>
                        <div class="col-md-2">
                            <input type="email" class="form-control" id="email" placeholder="Ingresa Telefono" name="email">
                        </div>
                    </div>
                    <div class="row">
                    <br>
                        <label class="control-label col-sm-1" for="email"><b>E-mail:</b></label>
                        <div class="col-md-5">
                            <input type="email" class="form-control" id="email" placeholder="Ingresa e-mail" name="email">
                        </div>
                    </div>
                        <br>
                    <div class="row">        
                        <div class="col-sm-offset-3 ">
                            <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                    </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
</body>
</html>