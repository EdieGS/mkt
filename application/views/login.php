<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Marketing</title>

  <!-- Favicons -->
  <link href="template/img/favicon.png" rel="icon">
  <link href="template/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="template/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="template/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="template/css/style.css" rel="stylesheet">
  <link href="template/css/style-responsive.css" rel="stylesheet">
</head>
<body>
<div id="login-page">
    <div class="container">
      <form method="POST" class="form-login" action="<?php echo base_url();?>index.php/welcome/login_validation">
        <h2 class="form-login-heading">Inicio de Sesion</h2>
        <div class="login-wrap">
          <input type="text" name="nombre" class="form-control" placeholder="Nombre" autofocus>
          <br>
          <input type="password" name="contraseña" class="form-control" placeholder="Contraseña">
          <br>
          <br>
          <br>
          <button class="btn btn-theme btn-block" value="Login"  type="submit"><i class="fa fa-lock"></i>Iniciar Sesion</button>
          <hr>
        </div>  
      </form>
    </div>
  </div>
  <!-- js placed at the end of the document so the pages load faster -->
  
  
  <script>
    $.backstretch("<?php base_url()?>template/img/login-bg.jpg", {
      speed: 1500
    });
  </script>
</body>

</html>