<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Marketing</title>

  <!-- Favicons -->
  <link href="<?php echo base_url()?>template/img/favicon.png" rel="icon">
  <link href="<?php echo base_url()?>template/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url()?>template/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="<?php echo base_url()?>template/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>template/css/zabuto_calendar.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>template/lib/gritter/css/jquery.gritter.css" />
  <!-- Custom styles for this template -->
  <link href="<?php echo base_url()?>template/css/style.css" rel="stylesheet">
  <link href="<?php echo base_url()?>template/css/style-responsive.css" rel="stylesheet">
  <script src="<?php echo base_url()?>template/lib/chart-master/Chart.js"></script>
</head>

<body>
    <section id="container">