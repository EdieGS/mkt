<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function index()
	{	
		$this->load->view('login');
	}
	public function logeado(){
		$this->load->view('assets/header');
		$this->load->view('admin/madmin');
		$this->load->view('admin/dashboard');
		$this->load->view('assets/footer');
	}
	//Función para validar el usuario en el login
	public function login_validation(){
		$this->form_validation->set_rules('nombre','username','required');
		$this->form_validation->set_rules('contraseña','required');
		//Si el formulario corre entra al if
		if($this->form_validation->run()){
			//Se extrae nombre y contraseña del formulario
			$nombre = $this->input->post('nombre');
			$contraseña = $this->input->post('contraseña');
			//se verifica si el usuario(nombre, contraseña) existen en la base de datos
			$usuario = $this->admin->login_m($nombre,$contraseña);
			//Si la consulta regresa vacía se redirecciona al login de nuevo
			if(empty($usuario)){
				$this->session->set_flashdata('error','Nombre o contraseña invalidos');

				redirect(base_url());	
			}
			//Si la consulta regresa con datos se redirecciona a la pagina logeado vista(madmin.php)
			if(!empty($usuario)){
				//separo los datos de la consulta en las variables de sesion, el $tipo se consultó en el modelo
				$this->session->set_userdata('nombre',$nombre = $usuario['Nombre']);

				/* con esta variable 'tipo' se valida lo que se le va a mostrar a cada usuario 
				en la vista madmin en la parte del menu en el tiempo mientras encontramos otra forma de hacerlo*/
				$this->session->set_userdata('tipo',$tipo = $usuario['tipo']);

				redirect(base_url().'index.php/welcome/logeado');
			}
		}
	}


	public function campana (){
		$all = $this->admin->campañas_m();
		$this->session->set_userdata('campañas',$all);
		

		$this->load->view('assets/header');
		$this->load->view('admin/madmin');
		$this->load->view('admin/dashbcamp');
		$this->load->view('assets/footer');
	}
	function logout(){
		$this->session->sess_destroy();
		redirect(base_url());
	}
	public function empresa(){
		$all = $this->admin->empresas_m();
		$this->session->set_userdata('empresas',$all);
		$this->load->view('assets/header');
		$this->load->view('admin/madmin');
		$this->load->view('admin/dashempresa');
		$this->load->view('assets/footer');

	}
	function dashboardcm(){
		$this->load->view('assets/header');
		$this->load->view('admin/madmin');
		$this->load->view('cm/inicio');
		$this->load->view('assets/footer');
	}
	function pendientes(){
	    $datos['pendientes'] = $this->mkt->getpendientes();
		$this->load->view('assets/header');
		$this->load->view('admin/madmin');
		$this->load->view('cm/pendientes', $datos);
		$this->load->view('assets/footer');
	}
		function aceptadas(){
	    $datos['aceptadas'] = $this->mkt->getaceptadas();
		$this->load->view('assets/header');
		$this->load->view('admin/madmin');

		$this->load->view('cm/aceptadas', $datos);
		$this->load->view('assets/footer');
	}
		function rechazadas(){
	    $datos['rechazadas'] = $this->mkt->getrechazadas();
		$this->load->view('assets/header');
		$this->load->view('admin/madmin');

		$this->load->view('cm/rechazadas', $datos);
		$this->load->view('assets/footer');
	}
	function campanacm(){
		$this->load->view('assets/header');
		$this->load->view('admin/madmin');

		$this->load->view('cm/campanas');
		$this->load->view('assets/footer');
	}
	function feed(){
		$this->load->view('assets/header');
		$this->load->view('admin/madmin');

		$this->load->view('cm/view');
		$this->load->view('assets/footer');
	}
	function comentarios(){
		$this->load->view('cm/comen');

	}
	function likes(){

		$this->load->view('cm/likes');

	}
	function shares(){
		$this->load->view('cm/shares');

	}
}

