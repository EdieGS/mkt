<?php
class Admin extends CI_Model{	
	//Se realiza la consulta con los datos del login, para verificar la existencia de datos en la BD
	public function login_m($nombre,$contraseña)
	{	
		$this->db->where('nombre',$nombre);
        $this->db->where('contraseña',$contraseña);
        $usuario=$this->db->get('users'); 
        return $usuario->result_array()[0];
	}
	public function campañas_m (){
		
		$query=$this->db->get('campaña');
		return $query->result_array();

	}
	public function empresas_m (){
		$this->db->select('id,  nombre,  encargado, telefono, e-mail');
		$query=$this->db->get('empresa');
		return $query->result_array();

	}
}

