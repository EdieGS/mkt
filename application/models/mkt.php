<?php 
	/**
	* Model donde se realiazaran las consultas a la base de datos y se envian los datos recibidos 
	*al controlador donde posteriormente se les da salida a una vista 
	*/
	class mkt extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->database();
		}	

		  function getpendientes(){
		  	  $query = $this->db->get('pendientes');
        	  return $query->result_array();
		  }
		  function getaceptadas(){
		  	  $query = $this->db->get('aceptadas');
        	  return $query->result_array();
		  }
		  function getrechazadas(){
		  	  $query = $this->db->get('rechazadas');
        	  return $query->result_array();
		  }
	}	

 ?>